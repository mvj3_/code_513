  strip_tags("Strip <i>these</i> tags!")
  # => Strip these tags!

  strip_tags("<b>Bold</b> no more!  <a href='more.html'>See more here</a>...")
  # => Bold no more!  See more here...

  strip_tags("<div id='top-bar'>Welcome to my website!</div>")
  # => Welcome to my website!